<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
        //$this->load->database();
        $this->load->library('form_validation');
        $this->load->library('session');
	}

	public function index(){
    	$param['LangCode'] = 'en';
    	$this->load->model('Home_model');

        //Get homepage data
        $response = $this->Home_model->getHomePageDetailsWeb($param);
        
        if(isset($response['IsSuccess']) && $response['IsSuccess'] == 1){
            //set cuisines data
            $result['cuisineData'] = $response['Data']->cuisines;
            
            //set areas data
            $result['areasData'] = $response['Data']->area;
        }else{
            $result['cuisineData'] = "ERR";
            $result['areasData'] = "ERR";
        }
        $this->load->view('home', $result);
    }

    public function register(){
        $param['LangCode'] = 'en';
        $this->load->view('register');
    }

    public function verifyOtpRegisterUser(){
        $responseRegister = array('StatusCode'=>400);
        if($this->input->post('Email') && $this->input->post('Email') != '' && $this->input->post('Password') && $this->input->post('Password') != ''
             && $this->input->post('mobile') && $this->input->post('mobile') != '' && $this->input->post('otp') && $this->input->post('otp') != ''){
            // echo "<pre>";print_r($_POST);die;
            $this->load->model('Home_model');

            $mobileNo = $this->input->post('dialCode').$this->input->post('mobile');
            $param['mobile'] = $mobileNo;
            $param['otp'] = $this->input->post('otp');

            $response = $this->Home_model->verifyOtp($param); 

            if($response['IsSuccess'] == true){
                $param1['lang_code'] = 'en';
                $param1['Firstname'] = $this->input->post('Firstname');
                $param1['Lastname'] = $this->input->post('Lastname');
                $param1['Email'] = $this->input->post('Email');
                $param1['Password'] = $this->input->post('Password');
                $param1['Mobile'] = $mobileNo;

                if($this->input->post('getAlerts') && $this->input->post('getAlerts') == true){
                    $param1['alert'] = 1;
                }else{
                    $param1['alert'] = 0;
                }
                
                $response = $this->Home_model->CustomerRegistrationWeb($param1);       
                //echo "<pre>";print_r($response);die;                      
            }            
        }
        echo json_encode($response);
    }

    public function login(){
        $this->checkIsAlreadyLogin();
        
        $param['LangCode'] = 'en';
        $this->load->view('login');
    }

    public function loginUser(){
        $responseLogin = array('StatusCode'=>400);
        if($this->input->post('email') && $this->input->post('email') != ''){
            //echo "<pre>";print_r($_POST);die;
            $this->load->model('Home_model');

            $param['lang_code'] = 'en';
            $param['EmailId'] = $this->input->post('email');
            $param['Password'] = $this->input->post('password');

            $responseLogin = $this->Home_model->dologinWeb($param);       
            $responseLogin['redirectLogin'] = $_SERVER['HTTP_REFERER'];
            //echo "<pre>";print_r($responseLogin);die;
            if(isset($responseLogin['IsSuccess']) && $responseLogin['IsSuccess'] == 1){
                $this->session->set_userdata('isLogin', true);
                $this->session->set_userdata('loginCustId', $responseLogin['Data'][0]->customer_id);
                $this->session->set_userdata('loginCustEmail', $responseLogin['Data'][0]->email);
                $this->session->set_userdata('loginCustNumber', $responseLogin['Data'][0]->contact_number);
                $this->session->set_userdata('loginCustNameEmail', $responseLogin['Data'][0]->customer_name);
            }
            //echo "<pre>";print_r($responseLogin);die;                      
        }
        echo json_encode($responseLogin);
    }

    public function checkIsAlreadyLogin(){
        if($this->session->userdata('isLogin') && $this->session->userdata('isLogin') == 1){
            redirect(base_url());
            exit;
        }
    }

    public function logout(){
        session_destroy();
        redirect(base_url().'login');
    }

    public function cuisines(){
    	$param['LangCode'] = 'en';
    	$this->load->model('Home_model');

        //Get cuisions data
        $response = $this->Home_model->getBrowseByCuisinesWeb($param);       
        if(isset($response['IsSuccess']) && $response['IsSuccess'] == 1){            
            $result['cuisineData'] = $response['Data'];           
        }else{
            $result['cuisineData'] = "ERR";
        }
       
        $this->load->view('cuisines', $result);
    }

    public function areas(){
        $param['LangCode'] = 'en';
    	$this->load->model('Home_model');

        //Get areas data
        $response = $this->Home_model->getBrowseByAreaWeb($param);       
        if(isset($response['IsSuccess']) && $response['IsSuccess'] == 1){        
            $result['areasData'] = $response['Data'];            
        }else{
            $result['areasData'] = "ERR";
        }

        $this->load->view('areas', $result);
    }

    public function forgotpassword(){
        $this->checkIsAlreadyLogin();
        
        $param['LangCode'] = 'en';
        $this->load->view('forgotpassword');
    }

    public function generateOtp(){
        if($this->input->post('isGenerateOtp') && $this->input->post('isGenerateOtp') == 1){
            //echo "<pre>";print_r($_POST);die;
            $this->load->model('Home_model');

            if($this->input->post('source') && $this->input->post('source') == "register"){
                $mobileNo = $this->input->post('dialCode').$this->input->post('mobile');
                $param['mobile'] = $mobileNo;
            }else if($this->input->post('source') && $this->input->post('source') == "forgot password"){
                $mobileNo = $this->input->post('dialCode').$this->input->post('mobile');
                $param['mobile'] = $mobileNo;
            }

            
            $param['source'] = $this->input->post('source');

            $responseGenOtp = $this->Home_model->generateOtp($param); 
            //echo "<pre>";print_r($responseGenOtp);die;                      
        }
        echo json_encode($responseGenOtp);
    }

    public function resendOtp(){
        if($this->input->post('mobile') && $this->input->post('mobile') != ''){
            //echo "<pre>";print_r($_POST);die;
            $this->load->model('Home_model');

            $mobileNo = $this->input->post('dialCode').$this->input->post('mobile');
            $param['mobile'] = $mobileNo;
            $param['source'] = $this->input->post('source');

            $responseResendOtp = $this->Home_model->generateOtp($param); 
            //echo "<pre>";print_r($responseGenOtp);die;                      
        }
        echo json_encode($responseResendOtp);
    }

    public function verifyOtp(){
        if($this->input->post('otp') && $this->input->post('otp') != '' && $this->input->post('mobile') && $this->input->post('mobile') != ''){
            //echo "<pre>";print_r($_POST);die;
            $this->load->model('Home_model');

            $mobileNo = $this->input->post('dialCode').$this->input->post('mobile');
            $param['mobile'] = $mobileNo;
            $param['otp'] = $this->input->post('otp');

            $responseVerifyOtp = $this->Home_model->verifyOtp($param); 
            //echo "<pre>";print_r($responseVerifyOtp);die;                      
        }
        echo json_encode($responseVerifyOtp);
    }

    public function savePassword(){
        if($this->input->post('password') && $this->input->post('password') != '' && $this->input->post('mobile') && $this->input->post('mobile') != ''){
            //echo "<pre>";print_r($_POST);die;
            $this->load->model('Home_model');

            $mobileNo = $this->input->post('dialCode').$this->input->post('mobile');
            $param['mobile'] = $mobileNo;
            $param['password'] = $this->input->post('password');

            $responseVerifyOtp = $this->Home_model->savePassword($param); 
            //echo "<pre>";print_r($responseVerifyOtp);die;                      
        }
        echo json_encode($responseVerifyOtp);
    }

}

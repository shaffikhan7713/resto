<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
        //$this->load->database();
        $this->load->library('form_validation');
	}

    public function getCountries(){
    	$cArray = array(
    		array('id'=>1, 'name'=>'India'),
    		array('id'=>2, 'name'=>'Pak'),
    		array('id'=>3, 'name'=>'US'),
		);

		echo json_encode($cArray);
    }    
}

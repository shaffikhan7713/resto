<?php
class Servicemanager {

	public function callServiceGet($requestParams = array(), $callApiUrl='', $contentTypeHeaders=''){
        
        $obj =& get_instance();

        $curlHandle = curl_init();

        $serviceUrl = $obj->config->item('base_url').$callApiUrl;

        //curl_setopt($curlHandle, CURLOPT_URL, $serviceUrl);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        $headers = $this->_getHeaders($contentTypeHeaders);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 50);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);

        $getParams='';
        if (isset($requestParams['parameters']['post']) && !empty($requestParams['parameters']['post'])) {
            //$requestParams['parameters']['post']['accessToken'] = $_SESSION['accessToken'];
            $getParams = http_build_query($requestParams['parameters']['post']);
            $getParams = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $getParams);
            $getParams = str_replace('%5B%5D=', '=', $getParams);
        }

        //Log service url and post data params
        /*Logger::log($this->logfileName, "REFERER PAGE :".$_SERVER['HTTP_REFERER']);
        Logger::log($this->logfileName, "SERVICE URL :".$serviceUrl);
        Logger::log($this->logfileName, "GET PARAMS :".$getParams);
        Logger::log($this->logfileName, "HEADERS :".json_encode($headers));
        Logger::log($this->logfileName, "POSTMAN URL :".$serviceUrl."?".$getParams);*/     

        if($getParams != '')
        	curl_setopt($curlHandle, CURLOPT_URL, $serviceUrl."?".$getParams);
        else
        	curl_setopt($curlHandle, CURLOPT_URL, $serviceUrl);

    	//echo $serviceUrl."?".$getParams;die;

        $response = curl_exec($curlHandle);
        $info = curl_getinfo($curlHandle);
        //echo "<pre>";print_r($info);die;
        //Logger::log($this->logfileName, "CURL INFO :".json_encode($info));

        if ($response == '') {
            $_SESSION['serverStatus'] = 'DOWN';
            $response['responseCode'] = "error";
            $response['responseMessage'] = "Server not responding";
            $response = json_encode($response);
        } else if (curl_errno($curlHandle)) {
            $_SESSION['serverStatus'] = 'DOWN';
            $response['responseCode'] = "error";
            $response['responseMessage'] = "Http page error " . $info['http_code'];
            $response = json_encode($response);
        }

        $formattedResponse = json_decode($response);
        //Logger::log($this->logfileName, "FORMATTED RESPONSE :".  json_encode($formattedResponse));

        
        //$formattedResponse['httpcode'] = $info['http_code'];
        return $formattedResponse;
    }

    public function callServiceJson($requestParams = array(), $callApiUrl='', $contentTypeHeaders='', $method='POST'){
        $obj =& get_instance();

        $curlHandle = curl_init();

        //$serviceUrl = $obj->config->item('base_url').$callApiUrl;
        $baseuurl = 'https://therappo.com';
        $serviceUrl =  $baseuurl.$callApiUrl;

        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        $headers = $this->_getHeaders($contentTypeHeaders);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 50);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);

        $getParams='';
        if (isset($requestParams['parameters']['post']) && !empty($requestParams['parameters']['post'])) {
            //$requestParams['parameters']['post']['accessToken'] = $_SESSION['accessToken'];
            //$getParams = safe_json_encode($requestParams['parameters']['post']);
            $getParams = json_encode($requestParams['parameters']['post']);
        }

        /*Logger::log($this->logfileName, "REFERER PAGE :".$_SERVER['HTTP_REFERER']);
        Logger::log($this->logfileName, "SERVICE URL :".$serviceUrl);
        Logger::log($this->logfileName, "PARAMS :".$getParams);
        Logger::log($this->logfileName, "HEADERS :".json_encode($headers));*/

        curl_setopt($curlHandle, CURLOPT_URL,$serviceUrl);
        if($method == "POST")
            curl_setopt($curlHandle, CURLOPT_POST, 1);
        else
            curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS,$getParams);

        $response = curl_exec($curlHandle);
        $info = curl_getinfo($curlHandle);

        //Logger::log($this->logfileName, "CURL INFO :".json_encode($info));

        if ($response == '') {
            $_SESSION['serverStatus'] = 'DOWN';
            $response['responseCode'] = "error";
            $response['responseMessage'] = "Server not responding";
            $response = json_encode($response);
        } else if (curl_errno($curlHandle)) {
            $_SESSION['serverStatus'] = 'DOWN';
            $response['responseCode'] = "error";
            $response['responseMessage'] = "Http page error " . $info['http_code'];
            $response = json_encode($response);
        }

        $formattedResponse = (array)json_decode($response);
        
        //Logger::log($this->logfileName, "FORMATTED RESPONSE :".json_encode($formattedResponse));

        /*if (!empty($formattedResponse['errorCode']) && !in_array($formattedResponse['errorCode'], $skipErrorCodes)) {
            if ($formattedResponse['errorCode'] == 301 && $requestParams['parameters']['post']['flashproxy'] != 'true') {
                $this->nodeNotFoundHandler($requestParams['parameters']['post'], $formattedResponse);
            }
        }*/
        //$formattedResponse['httpcode'] = $info['http_code'];
        return $formattedResponse;
    }

    public function callServiceJson1($requestParams = array(), $callApiName='', $callApiUrl='', $contentTypeHeaders='', $method='POST'){
        $clientIp = getIPAddress();
        $requestParams['parameters']['post']['ipAddress'] = $clientIp;

        $curlHandle = curl_init();

        $serviceUrl = $this->prepareServiceUrl($callApiName, $callApiUrl);

        //curl_setopt($curlHandle, CURLOPT_URL, $serviceUrl);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        $headers = $this->_getHeaders($contentTypeHeaders);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlHandle, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 50);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);

        $getParams='';
        if (isset($requestParams['parameters']['post']) && !empty($requestParams['parameters']['post'])) {
            $requestParams['parameters']['post']['accessToken'] = $_SESSION['accessToken'];
            $getParams = safe_json_encode($requestParams['parameters']['post']);
        }

        Logger::log($this->logfileName, "REFERER PAGE :".$_SERVER['HTTP_REFERER']);
        Logger::log($this->logfileName, "SERVICE URL :".$serviceUrl);
        Logger::log($this->logfileName, "PARAMS :".$getParams);
        Logger::log($this->logfileName, "HEADERS :".json_encode($headers));

        curl_setopt($curlHandle, CURLOPT_URL,$serviceUrl);
        if($method == "POST")
            curl_setopt($curlHandle, CURLOPT_POST, 1);
        else
            curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS,$getParams);

        $response = curl_exec($curlHandle);
        $info = curl_getinfo($curlHandle);
        Logger::log($this->logfileName, "CURL INFO :".json_encode($info));

        if ($response == '') {
            $_SESSION['serverStatus'] = 'DOWN';
            $response['responseCode'] = "error";
            $response['responseMessage'] = "Server not responding";
            $response = json_encode($response);
        } else if (curl_errno($curlHandle)) {
            $_SESSION['serverStatus'] = 'DOWN';
            $response['responseCode'] = "error";
            $response['responseMessage'] = "Http page error " . $info['http_code'];
            $response = json_encode($response);
        }

        $formattedResponse = JsonFormatter::decode($response);
        Logger::log($this->logfileName, "FORMATTED RESPONSE :".json_encode($formattedResponse));

        if (!empty($formattedResponse['errorCode']) && !in_array($formattedResponse['errorCode'], $skipErrorCodes)) {
            if ($formattedResponse['errorCode'] == 301 && $requestParams['parameters']['post']['flashproxy'] != 'true') {
                $this->nodeNotFoundHandler($requestParams['parameters']['post'], $formattedResponse);
            }
        }
        $formattedResponse['httpcode'] = $info['http_code'];
        return $formattedResponse;
    }

    private function _getHeaders($contentType='') {
        if($contentType == "application/json"){
            return array("Cache-Control: no-cache", "Content-type: application/json");
        }else{
            return array("Cache-Control: no-cache", "Content-type: application/x-www-form-urlencoded;charset=UTF-8");
        }
    }
}

?>

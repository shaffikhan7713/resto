<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('servicemanager');
    }

    function home_model()
    {
        //parent::Model();
    }

    function getAllCuisines($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Get_details/AllCuisines');
        return $result;
    }

    function getAllArea($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Get_details/AllArea');
        return $result;
    }

    function CustomerRegistrationWeb($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Users/CustomerRegistrationWeb');
        return $result;
    }
    function dologinWeb($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Users/dologinWeb');
        return $result;
    }

    function getHomePageDetailsWeb($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Get_details/HomePageDetailsApp');
        return $result;
    }

    function BrowseByCuisinesWeb($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Get_details/BrowseByCuisinesWeb');
        return $result;
    }

    function getBrowseByAreaWeb($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Get_details/BrowseByAreaWeb');
        return $result;
    }

    function generateOtp($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Users/generate_otp');
        return $result;
    }

    function verifyOtp($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Users/verify_otp');
        return $result;
    }

    function savePassword($params = array())
    {
        $postparams['parameters']['post'] = $params;
        $result = $this->servicemanager->callServiceJson($postparams, '/Resto/Users/forgot_password');
        return $result;
    }
}

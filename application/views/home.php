<?php
  $this->load->view('header');
?>

 <main>
    <section class="bs-banner set-bg" data-img="<?php echo base_url(); ?>html/images/banner.jpg">
        <div class="container">
            <div class="banner-content">
                <div class="bs-form">
                    <div class="form-group transparent left-icon location-group">
                        <i class="icon-map-pin icon"></i>
                        <input type="text" value="Riyadh, Saudi Arabia" class="form-control" />
                    </div>
                </div>
                <h1 class="heading">
                    Find your table for any occasion
                </h1>
                <div class="search-component">
                    <div class="left-parent">
                        <div class="row ">
                            <div class="col-md-8 col-sm-9 col-xs-12">
                                <div class="bs-form inline clearfix">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group bg select-parent left-icon">
                                                <i class="icon-user icon"></i>
                                                <select class="form-control  select">
                                                    <option>04 People</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group bg left-icon">
                                                <i class="icon-calendar icon"></i>
                                                <input type="text" class="form-control datepicker" placeholder="dd/mm/yyyy" />
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group bg select-parent">
                                                <select class="form-control  select">
                                                    <option>07:00 pm</option>
                                                    <option>08:00 pm</option>
                                                    <option>09:00 pm</option>                          
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-3 bs-form col-xs-12">
                                <div class="form-group bg left-icon bs-search-box">
                                    <i class="icon-search icon"></i>
                                    <input type="text" class="form-control search-input" placeholder="Search Location. Restaurant, Cuisine" />
                                    <div class="search-list-parent">
                                        <ul class="link-parent">
                                            <li>
                                                <a href="#">
                                                    All Restaurant
                                                </a>
                                                <a href="#">
                                                    My Favorites
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="search-list">
                                            <div class="title">
                                                Last Search restaurants or cuisine
                                            </div>
                                            <ul class="list">
                                                <li>
                                                    <a href="#">
                                                        <div class="left-img">
                                                            <img src="<?php echo base_url(); ?>html/images/search-list.png" alt="serachlist" />
                                                        </div>
                                                        <div class="desc">
                                                            <div class="name">
                                                                Plum by Bent Chair
                                                            </div>
                                                            <div class="location">
                                                                Hanifa Valley Street, Jedah, Riyadh 12214
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="left-img">
                                                            <img src="<?php echo base_url(); ?>html/images/search-list.png" alt="serachlist" />
                                                        </div>
                                                        <div class="desc">
                                                            <div class="name">
                                                                Plum by Bent Chair
                                                            </div>
                                                            <div class="location">
                                                                Hanifa Valley Street, Jedah, Riyadh 12214
                                                            </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-parent">
                        <button class="btn big">Let's Go</button>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="lyt-wb bx-shadow top">
                <h3>Recently viewed</h3>
                <!-- Swiper -->
                <div class="swiper-container banner-silder" id="resent-slider">
                    <div class="swiper-wrapper banner-wrapper">
                        <a href="#" class="swiper-slide ctm-slide">
                            <div class="silder-img">
                                <img src="<?php echo base_url(); ?>html/images/rv-silde-1.jpg" alt="silder1" class="img-responsive">
                                <div class="bs-tag">Recomemnd</div>
                                <div class="bottom-left">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                            <div class="bs-swiper-info">
                                <h4>Hotel Creacent <span class="swiper-subhead">( 5 Restaurant )</span></h4>
                                <!--<p class="swiper-subhead">Cafe, Chinese, Italian, Continental, North Indian</p>
                                <p class="swiper-info"> Jeddah | $10 per person | Booked 10 times today </p>-->
                            </div>

                        </a>
                        <a href="#" class="swiper-slide ctm-slide">
                            <div class="silder-img">
                                <img src="<?php echo base_url(); ?>html/images/rv-silde-2.jpg" alt="silder2" class="img-responsive">
                                <div class="bs-tag">Recomemnd</div>
                                <div class="bottom-left">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>

                                </div>
                            </div>

                            <div class="bs-swiper-info">
                                <h4>Hotel Grand  <span class="swiper-subhead">( 10 Restaurant )</span></h4>
                                <!--<p class="swiper-subhead">Cafe, Chinese, Italian, Continental, North Indian</p>-->
                                <!--<p class="swiper-info"> Jeddah | $10 per person | Booked 10 times today </p>-->
                            </div>

                        </a>
                        <a href="#" class="swiper-slide ctm-slide">
                            <div class="silder-img">
                                <img src="<?php echo base_url(); ?>html/images/rv-silde-1.jpg" alt="silder3" class="img-responsive">
                                <div class="bs-tag">Recomemnd</div>
                                <div class="bottom-left">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>

                                </div>
                            </div>

                            <div class="bs-swiper-info">
                                <h4>Hotel Creacent  <span class="swiper-subhead">( 3 Restaurant )</span></h4>
                                <!--<p class="swiper-subhead">Cafe, Chinese, Italian, Continental, North Indian</p>-->
                                <!--<p class="swiper-info"> Jeddah | $10 per person | Booked 10 times today </p>-->
                            </div>

                        </a>
                        <a href="#" class="swiper-slide ctm-slide">
                            <img src="<?php echo base_url(); ?>html/images/rv-silde-1.jpg" alt="silder4" class="img-responsive">
                            <div class="bs-swiper-info">
                                <h4>Hotel Creacent  <span class="swiper-subhead">( 1 Restaurant )</span></h4>
                                <!--<p class="swiper-subhead">Cafe, Chinese, Italian, Continental, North Indian</p>-->
                                <!--<p class="swiper-info"> Jeddah | $10 per person | Booked 10 times today </p>-->
                            </div>

                        </a>

                    </div>

                </div>
                <div class="swiper-pagination"></div>
            </div>

        </div>

    </section>
    <section>
        <div class="container">
            <div class="lyt-wb bx-shadow middle">
                <h3>Featured Areas</h3>
                <?php if(isset($areasData) && is_array($areasData) && count($areasData) > 0) { ?>
                <!-- Swiper -->
                <div class="swiper-container banner-silder" id="featured-slider">
                    <div class="swiper-wrapper banner-wrapper">
                        <?php $i=1; foreach($areasData as $areaData){ ?>
                        <a href="<?php echo base_url(); ?>area/<?php echo $areaData->area_id; ?>" class="swiper-slide ctm-slide">
                            <img src="<?php echo $areaData->image; ?>" alt="silder<?php echo $i; ?>">
                            <div class="bs-swiper-info">
                                <h4><?php echo $areaData->area_name; ?>
                                    <span class="swiper-subhead">( <?php echo $areaData->area_wise_restaurant_count; ?> Restaurant )</span>
                                </h4>
                            </div>
                        </a>
                        <?php $i++; } ?>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
                <a href="<?php echo base_url(); ?>areas" class="btn lnk-vm">VIEW MORE</a>
                <?php } ?>
                
                
                <?php if(isset($areasData) && is_array($areasData) && count($areasData) <= 0) { ?>
                        <p>No restaurants found</p>
                <?php } ?>

                <?php if(isset($areasData) && $areasData == "ERR") { ?>
                    <p>Something went wrong in accessing data, Please try again.</p>
                <?php } ?>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="lyt-wb bx-shadow last">
                <h3>Popular Cuisines</h3>
                <?php if(isset($cuisineData) && is_array($cuisineData) && count($cuisineData) > 0) { ?>
                <!-- Swiper -->
                <div class="swiper-container banner-silder" id="popular-slider">
                    <div class="swiper-wrapper banner-wrapper">
                        <?php $k=1; foreach($cuisineData as $cData){  ?>
                            <a href="<?php echo base_url(); ?>cuisine/<?php echo $cData->cuisine_id; ?>" class="swiper-slide ctm-slide">
                                <img src="<?php echo $cData->image; ?>" class='cuisines-img' alt="silder<?php echo $k; ?>">
                                <div class="bs-swiper-info">
                                    <h4><?php echo $cData->cuisine_name; ?>
                                    <span class="swiper-subhead">( <?php echo $cData->restaurant_count; ?> Restaurant )</span></h4>
                                    <!--<p class="swiper-subhead">Lorem ipsum dolor</p>-->
                                </div>
                            </a>
                        <?php $k++; } ?>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
                <!-- <a href="#" class="btn lnk-vm">VIEW MORE</a> -->
                <a href="<?php echo base_url(); ?>cuisines" class="btn lnk-vm">VIEW MORE</a>
                <?php } ?>
                
                 <?php if(isset($cuisineData) && is_array($cuisineData) && count($cuisineData) <= 0) { ?>
                        <p>No restaurants found.</p>
                <?php } ?>

                <?php if(isset($cuisineData) && $cuisineData == "ERR") { ?>
                    <p>Something went wrong in accessing data, Please try again.</p>
                <?php } ?>
            </div>
        </div>
    </section>
</main>

<?php
  $this->load->view('footer');
?>
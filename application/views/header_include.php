<!-- header start -->
        <header class="bs-header clearfix transparent">
    <div class="container">
        <?php if($this->session->userdata('isLogin') && $this->session->userdata('isLogin') == 1){ ?>
        <div class="header-nav clearfix">
            <ul class="nav-list clearfix">
                <li class="notification">
                    <a href="#" class="icon-notification"></a>
                </li>
                <li class="profile">
                    <div class="dropdown bs-dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="<?php echo base_url(); ?>html/images/avatar.png" alt="My Profile">
                        </a>
                        <div class="dropdown-menu profile-dropdown" aria-labelledby="dropdownMenuLink">
                            <!--<a href="#"><i class="icon-wishlist"></i> Favorites</a>
                            <a href="#"><i class="icon-upcoming-courses"></i>Upcoming courses</a>
                            <a href="#"><i class="icon-booking"></i>Booking</a>
                            <a href="#"><i class="icon-booking"></i>Booking request</a>
                            <a href="#"><i class="icon-catalogue"></i>Catalogue</a>-->
                            <a href="<?php echo base_url(); ?>logout"><i class="icon-login"></i>Logout</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <?php } ?>
        
        <?php if(!$this->session->userdata('isLogin')){ ?>
                <div class="button-parent">
                    <a href="<?php echo base_url(); ?>login">
                        <button class="btn big login-btn">Login</button>
                    </a>
                </div>
        <?php } ?>
        <div class="bs-form">
            <div class="form-group select-parent">
                <select class="form-control select"> 
                    <option value="English">
                        English
                    </option>
                    <option value="Arabic">
                        Arabic
                    </option>
                </select>
            </div>
        </div>

        <a href="<?php echo base_url(); ?>" class="logo-parent">
            <img src="<?php echo base_url(); ?>html/images/logo.png" class="logo-white" alt="book a resto logo " />
            <img src="<?php echo base_url(); ?>html/images/logo-black.png" class="logo-black" alt="book a resto logo " />
        </a>
    </div>
</header>
        <!-- header end -->
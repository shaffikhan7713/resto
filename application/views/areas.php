<?php
$this->load->view('header');
?>
<main>
    <div class="bs-breadcrumb">
        <div class="container">
            <a href="<?php echo base_url(); ?>">Home</a> <a>Cuisines</a>
        </div>
    </div>
    <section class="lyt-content-sec lyt-cuisines">
        <div class="container">
            <div class="row">
                <?php if (isset($areasData) && is_array($areasData) && count($areasData) < 1) { ?>
                    <p>No restaurants found.</p>
                <?php } elseif (isset($areasData) && $areasData == "ERR") { ?>
                    <p>Something went wrong in accessing data, Please try again.</p>
                <?php } else { ?>
                    <?php foreach ($areasData as $aData) { ?>
                        <div class="col-md-4">
                            <div class="bs-card bx-shadow cm-radius">
                                <a href="<?php echo base_url(); ?>area/<?php echo $aData->area_id; ?>">
                                    <div class="cm-radius card-banner">
                                        <img src="<?php echo $aData->image; ?>" class='featured-area-img' alt="<?php echo $aData->area_name ?>">
                                    </div>
                                    <div class="card-content">
                                        <h4 class="thumb-heading"><?php echo $aData->area_name ?></h4>
                                        <p class="cm-dot"><?php echo $aData->area_wise_restaurant_count; ?> restaurant</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php }
            } ?>
            </div>
        </div>

    </section>
</main>
<?php
$this->load->view('footer');
?>
			<footer class="bs-footer">
			    <div class="container">
			        <div class="row">
			            <div class="col-md-3">
			                <div class="chefcan-info">
			                    <p><img src="<?php echo base_url(); ?>html/images/logo-black.png" alt="Book a resto logo" class="ftr-logo"></p>
			                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

			                </div>
			            </div>
			            <div class="col-md-5 col-md-offset-1">
			                <div class="quicklinks">
			                    <div class="row">
			                        <div class="col-xs-6">
			                            <h3 class="footer-title">
			                                Quick Links
			                            </h3>
			                            <ul>
			                                <li><a href="javascript:;">Home</a></li>
			                                <li><a href="javascript:;">My Booking</a></li>
			                                <li><a href="javascript:;">Wishlist</a></li>
			                                <li><a href="javascript:;">Terms and conditions</a></li>
			                                <li><a href="javascript:;">Privacy</a></li>
			                                <li><a href="javascript:;">Disclaimer</a></li>
			                            </ul>
			                        </div>
			                        <div class="col-xs-6">
			                            <h3 class="footer-title">
			                                Categories
			                            </h3>
			                            <ul>
			                                <li><a href="javascript:;">Arabian</a></li>
			                                <li><a href="javascript:;">North Indian</a></li>
			                                <li><a href="javascript:;">Italian</a></li>
			                                <li><a href="javascript:;">Chinese</a></li>
			                            </ul>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="col-md-3">
			                <div class="social">
			                    <h3>Follow us</h3>
			                    <p class="social-icons clearfix">
			                        <a href="javascript:;"><i class="icon-facebook"></i><span>Facebook</span></a>
			                        <a href="javascript:;"><i class="icon-linkedin"></i><span>LinkedIn</span></a>
			                        <a href="javascript:;"><i class="icon-twitter"></i><span>Twitter</span></a>
			                        <a href="javascript:;"><i class="icon-instagram"></i><span>Instagram</span></a>
			                        <a href="javascript:;"><i class="icon-youtube"></i><span>Youtube</span></a>
			                    </p>
			                </div>

			            </div>

			        </div>
			    </div>
			</footer>
			<!-- footer end -->
	    </div>
	</body>
</html>
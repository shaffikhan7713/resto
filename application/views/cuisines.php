<?php
$this->load->view('header');
?>
<main>
    <div class="bs-breadcrumb">
        <div class="container">
            <a href="<?php echo base_url(); ?>">Home</a> <a>Cuisines</a>
        </div>
    </div>
    <section class="lyt-content-sec lyt-cuisines">
        <div class="container">
            <div class="row">
                <?php if(isset($cuisineData) && is_array($cuisineData) && count($cuisineData) < 1) { ?>
                        <p>No restaurants found</p>
                <?php } else if(isset($cuisineData) && $cuisineData == "ERR") { ?>
                    <p>Something went wrong in accessing data, Please try again.</p>
                <?php } else if(isset($cuisineData) && is_array($cuisineData) && count($cuisineData) > 0) { 
                    foreach ($cuisineData as $cData) { ?>
                    <div class="col-md-4">
                        <div class="bs-card bx-shadow cm-radius">
                            <a href="<?php echo base_url(); ?>cuisine/<?php echo $cData->cuisine_id; ?>">
                                <div class="cm-radius card-banner">
                                    <img src="<?php echo $cData->image;?>" class='cuisines-img' alt="<?php echo $cData->cuisine_name?>">
                                    <div class="bs-tag">Recomended</div>

                                </div>
                                <div class="card-content">
                                    <h4 class="thumb-heading"><?php echo $cData->cuisine_name?></h4>
                                    <p class="cm-dot"><?php echo $cData->restaurant_count;?> restaurant</p>
                                    <!-- <p class="cm-dot cm-pipe">
                                        <span>Jeddah</span>
                                        <span>$10 per person </span>
                                        <span>Booked 10 times today</span>
                                    </p> -->

                                </div>
                            </a>
                        </div>
                    </div>
                <?php } } ?>
            </div>
        </div>

    </section>
</main>
<?php
$this->load->view('footer');
?>
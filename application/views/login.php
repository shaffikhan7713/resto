<?php
$this->load->view('header');
?>

<main>
    <h1 class="cm-hidden">
        Book a Resto Login
    </h1>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 set-bg" data-mob-img="<?php echo base_url(); ?>html/images/login-banner.jpg">
                    <div class="lyt-login">
                        <div class="bx-shadow">
                            <div class="row">
                                <div class="col-md-5 col-sm-4">
                                    <div class="set-bg login-banner hidden-xs" data-img="<?php echo base_url(); ?>html/images/login-banner.jpg">

                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-8">
                                    <div class="login-content bs-form regular">
                                        <div class="login-inner">
                                            <h3 class="heading">
                                                Login
                                            </h3>
                                            <p class="errSuccMsg"></p>
                                            <form name="loginForm" id="loginForm" method="post">
                                                <div class="form-group">
                                                    <div class="material">
                                                        <input type="text" class="form-control" name="email" id="email">
                                                        <label for="email" class="label-input">Email ID</label>
                                                    </div>
                                                    <div class="error emailErr"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="material">
                                                        <input type="password" class="form-control" name="password" id="password">
                                                        <label for="password" class="label-input">Password</label>
                                                    </div>
                                                    <div class="error passErr"></div>
                                                    <div class="input-link-button">
                                                        <a href="<?php echo base_url(); ?>forgotpassword" class="btn lnk-vm">
                                                            Forgot Password?
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="button-group btn-parent">
                                                    <button class="btn big-width" type="submit">
                                                                Sign In
                                                            </button>
                                                    <a href="<?php echo base_url(); ?>register" class="btn lnk-vm">
                                                                Sign Up
                                                            </a>
                                                </div>
                                                <!--<div class="or-seperator">
                                                    <span>or</span>
                                                </div>
                                                <div class="button-group social-btn">
                                                    <a href="#" class="btn btn-icon-white white-btn with-icon">
                                                        <i class="icon icon-google-plus"></i>
                                                        <span>
                                                                    Sign in with Google
                                                                </span>
                                                    </a>
                                                    <a href="#" class="btn btn-icon-white white-btn with-icon">
                                                        <i class="icon icon-facebook"></i>
                                                        <span>
                                                                    Sign in with Facebook
                                                                </span>
                                                    </a>
                                                </div>-->

                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript">
$('document').ready(function(){
    var showflag = 1;
    $("#loginForm").validate({
        rules: {
            email: {
                required:true,
                email: true,
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: { required: "Please enter email" },
            password: { required: "Please enter password" }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "email" )
                $(".emailErr").html(error);
            else if  (element.attr("name") == "password" )
                $(".passErr").html(error);
        },
        submitHandler: function(form) {
            $('.errSuccMsg').removeClass('error').html('');
            $.ajax({
                type: 'POST',
                data: $('#loginForm').serialize(),
                url: SITEURL+'/home/loginUser',
                success: function(result) {
                    var res = JSON.parse(result);                    
                    if(res.IsSuccess == 1){
                        $('.errSuccMsg').addClass('success').html(res.Message);
                        setTimeout(function() {
                            location.href = res.redirectLogin;
                        }, 1000);
                        
                    }else if(res.IsSuccess != 1){
                        $('.errSuccMsg').addClass('error').html(res.Message);
                    }else{
                        $('.errSuccMsg').addClass('error').html("Something went wrong, Please try again.");
                    }
                }

            });
        }
    });
});
</script>

<?php
$this->load->view('footer');
?>
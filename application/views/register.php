<?php
$this->load->view('header');
?>

<main>
    <h1 class="cm-hidden">
        Book a Resto Login
    </h1>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 set-bg" data-mob-img="<?php echo base_url(); ?>html/images/login-banner.jpg">
                    <div class="lyt-login">
                        <div class="bx-shadow">
                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                    <div class="set-bg login-banner hidden-xs" data-img="<?php echo base_url(); ?>html/images/login-banner.jpg">

                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-7 registerDiv">
                                    <div class="login-content bs-form regular">
                                        <div class="login-inner">
                                            <h3 class="heading">
                                                Sign Up
                                            </h3>
                                            <p class="errSuccMsg"></p>
                                            <form name="registerForm" id="registerForm" method="post">
                                                <div class="row">
                                                    <div class=" col-sm-6">
                                                        <div class="form-group">
                                                            <div class="material">
                                                                <input type="text" class="form-control" name="Firstname" id="Firstname" />
                                                                <label for="fname" class="label-input">First Name</label>
                                                            </div>
                                                            <div class="error fnameErr"></div>
                                                        </div>
                                                    </div>
                                                    <div class=" col-sm-6">
                                                        <div class="form-group">
                                                            <div class="material">
                                                                <input type="text" class="form-control" name="Lastname" id="Lastname" />
                                                                <label for="lname" class="label-input">Last Name</label>
                                                            </div>
                                                            <div class="error lnameErr"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="flag-input">
                                                        <input type="text" class="form-control" name="telephone" id="telephone" placeholder="Mobile" maxlength="15" />
                                                        <input type="hidden" name="dialCode" id="dialCode" value="" />
                                                    </div>
                                                    <div class="error mobileErr"></div>
                                                </div>
                                                <div class="alert-parent">
                                                    <label class="bs-switch">
                                                        <input type="checkbox" id="getAlerts" name="getAlerts">
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <div class="description">Get booking & waitlist alerts on your phone</div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="material">
                                                        <input type="email" class="form-control" name="Email" id="Email" />
                                                        <label for="email" class="label-input">Email </label>
                                                    </div>
                                                    <div class="error emailErr"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="material">
                                                        <input type="password" class="form-control" name="Password" id="Password" />
                                                        <label for="password" class="label-input">Password </label>
                                                    </div>
                                                    <div class="error passErr"></div>
                                                </div>
                                                <!--<div class="form-group">
                                                    <div class="material">
                                                        <input type="text" class="form-control" name="Mobile" id="Mobile" />
                                                        <label for="mobile" class="label-input">Mobile </label>
                                                    </div>
                                                    <div class="error mobileErr"></div>
                                                </div>-->
                                                <div class="terms-desc">
                                                    Selecting "Create Account" you are agreeing to the terms and conditions of the Book a Resto <a href="#" class="">User Agreement</a> and <a href="#">Privacy Policy.</a>
                                                </div>
                                                <div class="button-group btn-parent">
                                                    <button class="btn" type="submit">
                                                        Create Account
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                                <!-- verification otp -->
                                <div class="col-md-7 col-sm-7 verifyOtpDiv" style="display:none;">
                                    <div class="login-content bs-form regular">
                                        <div class="login-inner">
                                            <div>
                                                <a href="javascript:void(0);" class="btn lnk-vm back verifyOtpBackBtn">
                                                    <i class="icon icon-chevron-left"></i> Back
                                                </a>
                                            </div>
                                            <h3 class="heading">
                                                Verification Code
                                            </h3>
                                            <p class="errSuccMsgVerifyOtp"></p>
                                            <div class="desc">
                                                <div>Please type the verification code sent to </div>
                                                <div class="bold mobNoDiv"></div>
                                            </div>
                                            <form name="verifyOtpForm" id="verifyOtpForm">
                                                <div class="form-group">
                                                    <div class="material">
                                                        <input type="number" class="form-control" id="otp" name="otp" />
                                                        <input type="hidden" class="form-control" name="source" value="verify otp" />
                                                        <label for="number" class="label-input">One Time Password</label>
                                                    </div>
                                                    <div class="error otpErr"></div>
                                                </div>
                                                <div class="button-group btn-parent">
                                                    <button class="btn big-width" type="submit">
                                                        Submit
                                                    </button>
                                                    <a class="btn lnk-vm resendOtpBtn">
                                                        Resend OTP
                                                    </a>
                                                </div>
                                                <p class="errSuccMsgResend"></p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript">
$('document').ready(function(){
    // International numbers start
    $("#telephone").intlTelInput({
        separateDialCode: true
    });

    var input = document.querySelector("#telephone");
    var countryData = $("#telephone").intlTelInput("getSelectedCountryData");
    $('#dialCode').val(countryData.dialCode);
    //console.log(countryData);


    input.addEventListener("countrychange", function() {
      countryData = $("#telephone").intlTelInput("getSelectedCountryData");
      $('#dialCode').val(countryData.dialCode);
      //console.log(countryData.dialCode);
    });
    // International numbers end

    /*var showflag = 1;
    $("#registerForm").validate({
        rules: {
            Firstname: {
                required: true,
            },
            Lastname: {
                required: true,
            },            
            Email: {
                required:true,
                email: true,
            },
            Password: {
                required: true,
            },
            telephone: {
                required: true,
                number: true
            }
        },
        messages: {
            Firstname: { required: "Please enter first name" },
            Lastname: { required: "Please enter last name" },
            Email: { required: "Please enter email" },
            Password: { required: "Please enter password" },
            telephone: { required: "Please enter mobile number" }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "Firstname" )
                $(".fnameErr").html(error);
            else if  (element.attr("name") == "Lastname" )
                $(".lnameErr").html(error);
            else if  (element.attr("name") == "Email" )
                $(".emailErr").html(error);
            else if  (element.attr("name") == "Password" )
                $(".passErr").html(error);
            else if  (element.attr("name") == "telephone" )
                $(".mobileErr").html(error);
        },
        submitHandler: function(form) {
            $.ajax({
                type: 'POST',
                data: $('#registerForm').serialize(),
                url: '<?php echo base_url(); ?>home/registerUser',
                success: function(result) {
                    var res = JSON.parse(result);                    
                    if(res.IsSuccess == 1){
                        $('.errSuccMsg').addClass('success').html(res.Message);
                        setTimeout(function() {
                            location.href = SITEURL+'home/login';
                        }, 1000);
                        
                    }else if(res.IsSuccess != 1){
                        if(res.Message == "Mobile number already exists"){
                            $(".mobileErr").html("Mobile number already exists");
                        }else if(res.Message == "Email already exists"){
                            $(".emailErr").html("Email already exists");
                        }else{
                            $('.errSuccMsg').addClass('error').html(res.Message);
                        }
                    }else{
                        $('.errSuccMsg').addClass('error').html("Something went wrong, Please try again.");
                    }
                }

            });
        }
    });*/

    var regFlag=1;
    $("#registerForm").validate({
        rules: {
            Firstname: {
                required: true,
            },
            Lastname: {
                required: true,
            },            
            Email: {
                required:true,
                email: true,
            },
            Password: {
                required: true,
            },
            telephone: {
                required: true,
                number: true,
                maxlength: 15
            }
        },
        messages: {
            Firstname: { required: "Please enter first name" },
            Lastname: { required: "Please enter last name" },
            Email: { required: "Please enter email" },
            Password: { required: "Please enter password" },
            telephone: { required: "Please enter mobile number" }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "Firstname" )
                $(".fnameErr").html(error);
            else if  (element.attr("name") == "Lastname" )
                $(".lnameErr").html(error);
            else if  (element.attr("name") == "Email" )
                $(".emailErr").html(error);
            else if  (element.attr("name") == "Password" )
                $(".passErr").html(error);
            else if  (element.attr("name") == "telephone" )
                $(".mobileErr").html(error);
        },
        submitHandler: function(form) {
            if(regFlag == 1){
                regFlag = 0;
                $.ajax({
                    type: 'POST',
                    //data: $('#registerForm').serialize(),
                    data: {dialCode: $('#dialCode').val(), mobile: $('#telephone').val(), source: 'register', 'isGenerateOtp': 1},
                    url: '<?php echo base_url(); ?>home/generateOtp',
                    success: function(result) {
                        regFlag = 1;
                        var res = JSON.parse(result);                    
                        if(res.IsSuccess == 1){
                            $('.registerDiv').hide();
                            $('.verifyOtpDiv').show();

                            var mobNo = "<b>+</b>"+$('#dialCode').val()+' '+$('#telephone').val();
                            $('.mobNoDiv').html(mobNo);                            
                            $('#otp').val('');                         
                        }else if(res.IsSuccess != 1){
                            $('.errSuccMsg').addClass('error').html(res.Message);
                        }else{
                            $('.errSuccMsg').addClass('error').html("Something went wrong in generating OTP, Please try again.");
                        }
                    }
                });
            }
        }
    });

    var resendOtpflag = 1;
    $(".resendOtpBtn").click(function(){
        if(resendOtpflag == 1){                
            resendOtpflag = 0;
            $(".mobileErr").html('');
            $.ajax({
                type: 'POST',
                data: {dialCode: $('#dialCode').val(), mobile: $('#telephone').val(), source: 'resend otp'},
                url: SITEURL+'/home/resendOtp',
                success: function(result) {
                    resendOtpflag = 1;
                    var res = JSON.parse(result);                    
                    if(res.IsSuccess == 1){
                        $('.errSuccMsgResend').addClass('success').html("OTP resend successfully").show().fadeOut(3000);
                    }else if(res.IsSuccess != 1){
                        $('.errSuccMsgResend').addClass('error').html(res.Message).show();
                    }else{
                        $('.errSuccMsgResend').addClass('error').html("Something went wrong in resending OTP, Please try again.").show();
                    }
                }
            });
        }
    });  

    var verifyOtpflag = 1;
    $("#verifyOtpForm").validate({      
        rules: {
            otp: {
                required: true,
                number: true,
                minlength: 5
            }
        },
        messages: {
            otp: { required: "Please enter OTP" }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == "otp" )
                $(".otpErr").html(error);
        },
        submitHandler: function(form) {
            if(verifyOtpflag == 1){
                verifyOtpflag = 0;
                $(".otpErr").html('');
                $.ajax({
                    type: 'POST',
                    data: {
                        Firstname: $('#Firstname').val(), 
                        Lastname: $('#Lastname').val(),
                        Email: $('#Email').val(), 
                        Password: $('#Password').val(), 
                        dialCode: $('#dialCode').val(),
                        mobile: $('#telephone').val(), 
                        otp: $('#otp').val(),
                    },
                    url: SITEURL+'/home/verifyOtpRegisterUser',
                    success: function(result) {
                        verifyOtpflag = 1;
                        var res = JSON.parse(result);                    
                        if(res.IsSuccess == 1){
                            $('.errSuccMsgVerifyOtp').addClass('success').html(res.Message);
                            setTimeout(function() {
                                location.href = SITEURL+'home/login';
                            }, 1000);
                            
                        }else if(res.IsSuccess != 1){
                            $(".otpErr").addClass('error').html(res.Message);
                        }else{
                            $(".otpErr").addClass('error').html("Something went wrong, Please try again.");
                        }
                    }

                });
            }
        }
    });

    $('.verifyOtpBackBtn').click(function(){
        $('.registerDiv').show();
        $('.verifyOtpDiv').hide();     
    });
});
</script>

<?php
$this->load->view('footer');
?>
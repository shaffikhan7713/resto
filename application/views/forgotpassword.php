<?php
$this->load->view('header');
?>

<main>
    <h1 class="cm-hidden">
        Book a Resto Login
    </h1>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 set-bg col-sm-12" data-mob-img="<?php echo base_url(); ?>html/images/login-banner.jpg">
                    <div class="lyt-login">
                        <div class="bx-shadow">
                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                    <div class="set-bg login-banner hidden-xs" data-img="<?php echo base_url(); ?>html/images/login-banner.jpg">

                                    </div>
                                </div>
                                <!-- generate otp -->
                                <div class="col-md-7 col-sm-7 generateOtpDiv">
                                    <div class="login-content bs-form regular">
                                        <div class="login-inner">
                                            <div>
                                                <a href="<?php echo base_url(); ?>login" class="btn lnk-vm back">
                                                    <i class="icon icon-chevron-left"></i> Back
                                                </a>
                                            </div>
                                            <h3 class="heading">
                                                Forgot Password
                                            </h3>
                                            <div class="desc">
                                                <div>Enter the mobile number associated with your account. </div>
                                                <div class="bold">We will send you an OTP to verify</div>
                                            </div>
                                            <form name="generateOtpForm" id="generateOtpForm">
                                                <div class="form-group">
                                                    <div class="flag-input">
                                                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" maxlength="15" />
                                                        <input type="hidden" class="form-control" name="source" value="forgot password" />
                                                        <input type="hidden" class="form-control" name="isGenerateOtp" value="1" />
                                                        <input type="hidden" name="dialCode" id="dialCode" value="" />
                                                    </div>
                                                    <div class="error mobileErr"></div>
                                                </div>
                                                <div class="btn-parent">
                                                    <button class="btn big-width" type="submit">
                                                        Send
                                                    </button>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <!-- verification otp -->
                                <div class="col-md-7 col-sm-7 verifyOtpDiv" style="display:none;">
                                    <div class="login-content bs-form regular">
                                        <div class="login-inner">
                                            <div>
                                                <a href="javascript:void(0);" class="btn lnk-vm back verifyOtpBackBtn">
                                                    <i class="icon icon-chevron-left"></i> Back
                                                </a>
                                            </div>
                                            <h3 class="heading">
                                                Verification Code
                                            </h3>
                                            <div class="desc">
                                                <div>Please type the verification code sent to </div>
                                                <div class="bold mobNoDiv"></div>
                                            </div>
                                            <form name="verifyOtpForm" id="verifyOtpForm">
                                                <div class="form-group">
                                                    <div class="material">
                                                        <input type="number" class="form-control" id="otp" name="otp" />
                                                        <input type="hidden" class="form-control" name="source" value="verify otp" />
                                                        <label for="number" class="label-input">One Time Password</label>
                                                    </div>
                                                    <div class="error otpErr"></div>
                                                </div>
                                                <div class="button-group btn-parent">
                                                    <button class="btn big-width" type="submit">
                                                        Submit
                                                    </button>
                                                    <a class="btn lnk-vm resendOtpBtn">
                                                        Resend OTP
                                                    </a>
                                                </div>
                                                <p class="errSuccMsg"></p>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <!-- save password -->
                                <div class="col-md-7 col-sm-7 savePassDiv" style="display:none;">
                                    <div class="login-content bs-form regular">
                                        <div class="login-inner">
                                            <div>
                                                <a href="javascript:void(0);" class="btn lnk-vm back savePassBackBtn">
                                                    <i class="icon icon-chevron-left"></i> Back
                                                </a>
                                            </div>
                                            <h3 class="heading">
                                                Reset Password
                                            </h3>
                                            <p class="errSuccMsgSavePass"></p>
                                            <div class="desc">
                                                Enter your new password below, we're just being extra safe.
                                            </div>
                                            <form name="savePassForm" id="savePassForm">
                                                <div class="form-group">
                                                    <div class="material">
                                                        <input type="text" class="form-control" id="npass" name="npass" />
                                                        <label for="npass" class="label-input">New Password</label>
                                                    </div>
                                                    <div class="error npassErr"></div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="material">
                                                        <input type="text" class="form-control" id="cpass" name="cpass" />
                                                        <label for="cpass" class="label-input">Confirm Password</label>
                                                    </div>
                                                    <div class="error cpassErr"></div>
                                                </div>
                                                <div class="button-group btn-parent">
                                                    <button class="btn big-width" type="submit">
                                                        Submit
                                                    </button>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript">
$('document').ready(function(){
    // International numbers start
    $("#mobile").intlTelInput({
        separateDialCode: true
    });

    var input = document.querySelector("#mobile");
    var countryData = $("#mobile").intlTelInput("getSelectedCountryData");
    $('#dialCode').val(countryData.dialCode);
    //console.log(countryData);

    input.addEventListener("countrychange", function() {
      countryData = $("#mobile").intlTelInput("getSelectedCountryData");
      $('#dialCode').val(countryData.dialCode);
    });
    // International numbers end

    var generateOtpflag = 1;
    $("#generateOtpForm").validate({       
        rules: {
            mobile: {
                required: true,
                number: true,
                minlength: 10
            }
        },
        messages: {
            mobile: { required: "Please enter mobile number" }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == "mobile" )
                $(".mobileErr").html(error);
        },
        submitHandler: function(form) {
            if(generateOtpflag == 1){
                generateOtpflag = 0;
                $(".mobileErr").html('');
                $.ajax({
                    type: 'POST',
                    data: $('#generateOtpForm').serialize(),
                    url: SITEURL+'home/generateOtp',
                    success: function(result) {
                        generateOtpflag = 1;
                        var res = JSON.parse(result);                    
                        if(res.IsSuccess == 1){
                            $('.generateOtpDiv').hide();
                            var mobNo = "<b>+</b>"+$('#dialCode').val()+' '+$('#mobile').val();
                            $('.mobNoDiv').html(mobNo);  
                            $('.verifyOtpDiv').show();
                            $('#otp').val('');
                            $('.savePassDiv').hide();  
                        }else if(res.IsSuccess != 1){
                            $(".mobileErr").html(res.Message);
                        }else{
                            $(".mobileErr").html("Something went wrong in generating OTP, Please try again.");
                        }
                    }

                });
            }
        }
    });
    
    var resendOtpflag = 1;
    $(".resendOtpBtn").click(function(){
        if(resendOtpflag == 1){                
            resendOtpflag = 0;
            $(".mobileErr").html('');
            $.ajax({
                type: 'POST',
                data: {mobile: $('#mobile').val(), source: 'resend otp'},
                url: SITEURL+'home/resendOtp',
                success: function(result) {
                    resendOtpflag = 1;
                    var res = JSON.parse(result);                    
                    if(res.IsSuccess == 1){
                        $('.errSuccMsg').addClass('success').html("OTP resend successfully").show().fadeOut(3000);
                    }else if(res.IsSuccess != 1){
                        $('.errSuccMsg').addClass('error').html(res.Message).show();
                    }else{
                        $('.errSuccMsg').addClass('error').html("Something went wrong in resending OTP, Please try again.").show();
                    }
                }
            });
        }
    });


    var verifyOtpflag = 1;
    $("#verifyOtpForm").validate({      
        rules: {
            otp: {
                required: true,
                number: true,
                minlength: 5
            }
        },
        messages: {
            otp: { required: "Please enter OTP" }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == "otp" )
                $(".otpErr").html(error);
        },
        submitHandler: function(form) {
            if(verifyOtpflag == 1){
                verifyOtpflag = 0;
                $(".otpErr").html('');
                $.ajax({
                    type: 'POST',
                    data: {mobile: $('#mobile').val(), dialCode: $('#dialCode').val(), otp: $('#otp').val()},
                    url: SITEURL+'home/verifyOtp',
                    success: function(result) {
                        verifyOtpflag = 1;
                        var res = JSON.parse(result);                    
                        if(res.IsSuccess == 1){
                            $('.generateOtpDiv').hide();
                            var mobNo = "<b>+</b>"+$('#dialCode').val()+' '+$('#mobile').val();
                            $('.mobNoDiv').html(mobNo); 
                            $('.verifyOtpDiv').hide();

                            $('#npass').val('');
                            $('#cpass').val('');

                            $('.savePassDiv').show();  
                        }else if(res.IsSuccess != 1){
                            $(".otpErr").html(res.Message);
                        }else{
                            $(".otpErr").html("Something went wrong in verifying OTP, Please try again.");
                        }
                    }

                });
            }
        }
    });


    var savePassflag = 1;
    $("#savePassForm").validate({        
        rules: {
            npass: {
                required: true
            },
            cpass: {
                required: true,
                equalTo: "#npass"
            }
        },
        messages: {
            npass: { required: "Please enter new password" },
            cpass: { required: "Please enter confirm password" }
        },
        errorPlacement: function(error, element) {
            if(element.attr("name") == "npass" )
                $(".npassErr").html(error);
            else if(element.attr("name") == "cpass" )
                $(".cpassErr").html(error);
        },
        submitHandler: function(form) {
            if(savePassflag == 1){      
                savePassflag = 0;
                $('.errSuccMsgSavePass').html("");
                $.ajax({
                    type: 'POST',
                    data: {mobile: $('#mobile').val(), dialCode: $('#dialCode').val(), password: $('#npass').val()},
                    url: SITEURL+'home/savePassword',
                    success: function(result) {
                        savePassflag = 1;
                        var res = JSON.parse(result);                    
                        if(res.IsSuccess == 1){
                            $('.errSuccMsgSavePass').addClass('success').html("Password updated successfully");
                            setTimeout(function() {
                                location.href = SITEURL+'home/login';
                            }, 1000);
                        }else if(res.IsSuccess != 1){
                            $('.errSuccMsgSavePass').addClass('error').html(res.Message);
                        }else{
                            $('.errSuccMsgSavePass').addClass('error').html("Something went wrong in updating password, Please try again.");
                        }
                    }

                });
            }
        }
    });

    $('.verifyOtpBackBtn').click(function(){
        $('.generateOtpDiv').show();
        $('.verifyOtpDiv').hide();
        $('.savePassDiv').hide();        
    });

    $('.savePassBackBtn').click(function(){
        $('.generateOtpDiv').hide();
        $('.verifyOtpDiv').show();
        $('.savePassDiv').hide();        
    });
    
});
</script>

<?php
$this->load->view('footer');
?>
var winWidth,
    winHeight, mobile_breakbpoint = 767;

function get_height_width() {
    winWidth = $(window).width(),
        winHeight = $(window).height();
}

function set_height_width() {
    if ($('body').height() < winHeight) {
        $('.wh').outerHeight(winHeight);
    }
    $('.wh-min').css('min-height', winHeight);
    $('.ww').outerWidth(winWidth);
}

function set_background() {
    $('.set-bg').each(function() {
        if (typeof $(this).attr('data-mob-img') === 'undefined') {
            $(this).css({
                'background': 'url(' + $(this).attr('data-img') + ')',
                'background-size': 'cover'
            });
        } else {
            if (winWidth > mobile_breakbpoint) {
                if (typeof $(this).attr('data-img') != 'undefined') {
                    $(this).css({
                        'background': 'url(' + $(this).attr('data-img') + ')',
                        'background-size': 'cover'
                    });
                }
            } else {
                $(this).css({
                    'background': 'url(' + $(this).attr('data-mob-img') + ')',
                    'background-size': 'cover'
                });
            }
        }
    });
}

function custom_select() {
    $('.select').customSelect();
}

function search_box() {
    $('.bs-search-box').each(function() {

        element = $(this);
        element.find('.search-input').on('focus', function() {
            element.find('.search-list-parent').fadeIn();
        });
        element.find('.search-input').on('focusout', function() {
            element.find('.search-list-parent').fadeOut();
        });
    });
}

function swiper_call(element, slidesperview, rsslidesperview) {
    // $(element).each(function() {
    //     var swiperelement = $(this);
    //     var swiper = new Swiper(swiperelement, {
    //         //slidesPerView: 5,
    //         //spaceBetween: 20,
    //         //freeMode: true,
    //         pagination: {
    //             el: swiperelement.next('.swiper-pagination'),
    //             clickable: true,
    //         },
    //         // breakpoints: {
    //         //     767: {
    //         //         slidesPerView: rsslidesperview,
    //         //         spaceBetween: 10,
    //         //     }
    //         // }
    //     });
    // });

    var swiper = new Swiper(element, {
        slidesPerView: slidesperview,
        spaceBetween: 30,
        pagination: {
            el: $(element).next('.swiper-pagination'),
            clickable: true,
        },
        breakpoints: {
            767: {
                slidesPerView: rsslidesperview,
                spaceBetween: 10,
            }
        }
    });

}

function noUiSliderline() {
    if ($('#distance-slider').length > 0) {
        var slider = document.getElementById('distance-slider');
        noUiSlider.create(slider, {
            start: [20, 80],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
    }

}

function datepicker() {
    $('.datepicker').datepicker({});
}



var map;

function initMap() {
    if ($('#map').length >= 1) {

        map = new google.maps.Map(
            document.getElementById('map'), {
                center: new google.maps.LatLng(-33.91722, 151.23064),
                zoom: 16
            });

        var iconBase =
            'https://www.creativewebo.com/clients/bar-website/assets/images/';

        var icons = {
            icon1: {
                icon: iconBase + 'map-pointer.png'
            }
        };

        var features = [{
                position: new google.maps.LatLng(-33.91722, 151.23064),
                type: 'icon1'
            },
            // {
            //     position: new google.maps.LatLng(-33.91539, 151.22820),
            //     type: 'icon1'
            // }, 
            // {
            //     position: new google.maps.LatLng(-33.91747, 151.22912),
            //     type: 'icon1'
            // }

        ];

        // Create markers.
        for (var i = 0; i < features.length; i++) {
            var marker = new google.maps.Marker({
                position: features[i].position,
                icon: icons[features[i].type].icon,
                map: map
            });
        };
    }

}

function filter_formobile() {
    $('.filter-click').click(function() {
        $('.lp-left-info ').addClass('active');
    });
    $('.btn-done').click(function(e) {
        e.preventDefault();
        $('.lp-left-info ').removeClass('active');
    });
}

function material_input() {
    var input = $('.material .form-control');
    input.focus(function() {
        $(this).parent().addClass('focus');
    });
    input.focusout(function() {
        if ($(this).parent().hasClass('readonly') == false) {
            if ($(this).val() == '') {
                $(this).parent().removeClass('focus');
            }
        }
    });
}

function edit_input() {
    $('.inputedit').click(function() {
        $(this).hide();
        $(this).next().removeAttr('readonly');
    });
}

function multiple_map() {
    var map;

    function initMap() {
        map = new google.maps.Map(
            document.getElementById('map-multiple-pin'), {
                center: new google.maps.LatLng(-33.91722, 151.23064),
                zoom: 16
            });

        var iconBase =
            'https://www.creativewebo.com/clients/bar-website/assets/images/';

        var icons = {
            icon1: {
                icon: iconBase + 'map-pointer.png'
            }
        };

        var features = [{
            position: new google.maps.LatLng(-33.91721, 151.22630),
            type: 'icon1'
        }, {
            position: new google.maps.LatLng(-33.91539, 151.22820),
            type: 'icon1'
        }, {
            position: new google.maps.LatLng(-33.91747, 151.22912),
            type: 'icon1'
        }];

        // Create markers.
        for (var i = 0; i < features.length; i++) {
            var marker = new google.maps.Marker({
                position: features[i].position,
                icon: icons[features[i].type].icon,
                map: map
            });
        };
    }

}

function images_popup() {
    var groups = {};
    $('.galleryItem').each(function() {
        var id = parseInt($(this).attr('data-group'), 10);

        if (!groups[id]) {
            groups[id] = [];
        }

        groups[id].push(this);
    });


    $.each(groups, function() {

        $(this).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            gallery: { enabled: true }
        })

    });
}

function read_more() {

    $('.read-more').click(function() {
        if ($(this).closest('.tab-para').hasClass('open') == true) {
            $(this).closest('.tab-para').find('.more').hide();
            $(this).closest('.tab-para').find('.dots').hide();
            $(this).html('read more');
            $(this).closest('.tab-para').removeClass('open');

        } else {
            $(this).closest('.tab-para').find('.more').show();
            $(this).closest('.tab-para').find('.dots').hide();
            $(this).closest('.tab-para').addClass('open');
            $(this).html('read less');
        }
    });
}

function tab_scroll() {
    $('.nav-tabs').each(function() {
        var tab_width = 0,
            tab_parent = $(this);
        $(this).find('li').each(function() {
            tab_width += $(this).innerWidth() + 10;
        });
        tab_parent.width(tab_width + 'px');
    });
}

function validation() {
    $("#login-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());


        },
        submitHandler: function(form) {
            return false;
        }
    });
}

function intinput() {
    $("#telephone").intlTelInput({
        separateDialCode: true
    });

}
$(function() {
    get_height_width();
    set_height_width();
    set_background();
    custom_select();
    //swiper_call();
    swiper_call('#resent-slider', 3, 1);
    swiper_call('#featured-slider', 4, 1);
    swiper_call('#popular-slider', 4, 1);
    search_box();
    datepicker();
    noUiSliderline();
    filter_formobile();
    material_input();
    multiple_map();
    images_popup();
    read_more();
    edit_input();
    validation();
    //intinput();
    //initMap();


});

$(window).load(function() {
    tab_scroll();
});